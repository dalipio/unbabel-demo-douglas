package com.demoproject.data

import android.arch.persistence.room.*

/**
 * Created by HAL-9000 on 22/08/2017.
 */
@Dao
interface PostDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertPerson(post: Post)

    @Update
    fun updatePerson(post: Post)

    @Delete
    fun deletePerson(vararg post: Post)

    @Query("SELECT * FROM post")
    fun getAllPosts(): List<Post>
}