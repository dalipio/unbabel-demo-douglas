package com.demoproject.data

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import android.arch.persistence.room.OnConflictStrategy.IGNORE

/**
 * Created by HAL-9000 on 21/08/2017.
 */
@Dao
interface PeopleDao {

    @Insert(onConflict = IGNORE)
    fun insertPerson(person: Person)

    @Update
    fun updatePerson(person: Person)

    @Delete
    fun deletePerson(vararg people: Person)

    @Query("SELECT * FROM Person ORDER BY firstName")
    fun listAll(): LiveData<List<Person>>
}