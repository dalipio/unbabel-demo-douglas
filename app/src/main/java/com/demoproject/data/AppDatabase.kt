package com.demoproject.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

/**
 * Created by HAL-9000 on 21/08/2017.
 */

@Database(
        entities = arrayOf(Person::class, Post::class),
        version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun peopleDao(): PostDao

    companion object {

        private val DB_NAME = "dbPeople"
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        DB_NAME)
                        .allowMainThreadQueries()
                        .build()
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}