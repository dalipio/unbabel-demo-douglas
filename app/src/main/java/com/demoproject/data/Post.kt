package com.demoproject.data

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

/**
 * Created by HAL-9000 on 22/08/2017.
 */
@Entity(tableName = "post")
data class Post(
        @PrimaryKey(autoGenerate = true) var id: Long = 0L,
        var title: String = "",
        var body: String = "") : Serializable