package com.demoproject.data

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

/**
 * Created by HAL-9000 on 21/08/2017.
 */
@Entity
data class Person(
        @PrimaryKey(autoGenerate = true) var id: Long = 0L,
        var firstName: String = "",
        var lastName: String = "",
        var age: Int = 0) : Serializable