package com.demoproject.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.demoproject.R
import com.demoproject.ctx
import com.demoproject.data.Post
import kotlinx.android.synthetic.main.item_post.view.*

/**
 * Created by HAL-9000 on 19/08/2017.
 */
class DetailAdapter(val posts: List<Post>, val itemClick: (Post) -> Unit) :
        RecyclerView.Adapter<DetailAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.ctx).inflate(R.layout.item_post, parent, false)
        return ViewHolder(view, itemClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindForecast(posts[position])
    }

    override fun getItemCount(): Int = posts.size

    class ViewHolder(view: View, val itemClick: (Post) -> Unit) : RecyclerView.ViewHolder(view) {

        fun bindForecast(post: Post) {
            //inline function | chapter 10.2
            with(post) {
                itemView.titlePost.text = title
                itemView.bodyPost.text = body
                itemView.setOnClickListener { itemClick(this) }
            }
        }
    }

}