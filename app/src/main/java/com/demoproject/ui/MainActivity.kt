package com.demoproject.ui

import android.arch.lifecycle.LifecycleRegistry
import android.arch.lifecycle.LifecycleRegistryOwner
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.demoproject.R
import com.demoproject.data.Post
import com.demoproject.server.DataMapper
import com.demoproject.server.DataRequest
import kotlinx.android.synthetic.main.activity_post.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


class MainActivity : AppCompatActivity(), LifecycleRegistryOwner {

    var lifecycleRegistry = LifecycleRegistry(this)
    lateinit var viewModel: PersonFormViewModel

    override fun getLifecycle(): LifecycleRegistry = lifecycleRegistry

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post)
        viewModel = ViewModelProviders.of(this).get(PersonFormViewModel::class.java)
        mainList.layoutManager = LinearLayoutManager(this)

        doAsync {
            val posts = DataMapper().getPostsObjectFromJson(DataRequest().executeFromPosts())
            for (post: Post in posts) viewModel.savePerson(post)
            uiThread {
                val model = ViewModelProviders.of(this@MainActivity).get(ListPeopleViewModel::class.java)

                for (post: Post in model.getPeople()) {
                    viewModel.savePerson(post)
                    Log.e("test", "listando post:" + post.title)
                }
                mainList.adapter = DetailAdapter(model.getPeople()) { showSimpleAlert() }
            }
        }
    }

    private fun showSimpleAlert() {

        val simpleAlert = AlertDialog.Builder(this@MainActivity).create()
        simpleAlert.setTitle("Alert")
        simpleAlert.setMessage("Show simple Alert")

        simpleAlert.setButton(AlertDialog.BUTTON_POSITIVE, "OK", {
            dialogInterface, i ->
            Unit
        })

        simpleAlert.show()
    }
}
