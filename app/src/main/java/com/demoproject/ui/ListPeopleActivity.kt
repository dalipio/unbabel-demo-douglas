package com.demoproject.ui

import android.arch.lifecycle.LifecycleRegistry
import android.arch.lifecycle.LifecycleRegistryOwner
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.demoproject.R
import com.demoproject.data.AppDatabase
import kotlinx.android.synthetic.main.activity_list_people.*


class ListPeopleActivity : AppCompatActivity(), LifecycleRegistryOwner {
    var lifecycleRegistry = LifecycleRegistry(this)

    override fun getLifecycle(): LifecycleRegistry = lifecycleRegistry

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_people)

        fab.setOnClickListener {
            PersonFormFragment().show(supportFragmentManager, "form")
        }

        val model = ViewModelProviders.of(this).get(ListPeopleViewModel::class.java)
//        model.getPeople().observe(this, Observer { people ->
//            listview.adapter = ArrayAdapter<Person>(
//                    this@ListPeopleActivity,
//                    android.R.layout.simple_list_item_1,
//                    people)
//
//            listview.setOnItemClickListener { _, _, position, _ ->
//                val person = people?.get(position)
//                if (person != null) {
//                    PersonFormFragment.newInstance(person).show(supportFragmentManager, "form")
//                }
//            }
//        })
    }

    override fun onDestroy() {
        AppDatabase.destroyInstance()
        super.onDestroy()
    }
}
