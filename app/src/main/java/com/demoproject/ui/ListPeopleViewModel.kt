package com.demoproject.ui

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.util.Log
import com.demoproject.data.AppDatabase
import com.demoproject.data.Post
import com.demoproject.data.PostDao

/**
 * Created by HAL-9000 on 21/08/2017.
 */
class ListPeopleViewModel(app: Application) : AndroidViewModel(app) {

    private var livePeople: List<Post>? = null

    var db: PostDao? = null

    private fun getDao(): PostDao? {
        if (db == null) {
            db = AppDatabase.getDatabase(getApplication())?.peopleDao()
        }
        return db
    }

    fun getPeople(): List<Post> {
        if (livePeople == null) {
            livePeople = getDao()?.getAllPosts()
            Log.e("test", "busquei os dados")
        }
        return livePeople as List<Post>
    }
}