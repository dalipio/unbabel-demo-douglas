package com.demoproject.ui

/**
 * Created by HAL-9000 on 21/08/2017.
 */
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.demoproject.R
import com.demoproject.data.Person
import kotlinx.android.synthetic.main.alert_fragment.*

class PersonFormFragment : DialogFragment() {

    lateinit var person : Person
    lateinit var viewModel : PersonFormViewModel

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        viewModel = ViewModelProviders.of(this).get(PersonFormViewModel::class.java)
        person = arguments?.getSerializable(EXTRA_PERSON) as? Person ?: Person()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.alert_fragment, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        edtFirstName.setText(person.firstName)
        edtLastName.setText(person.lastName)
        edtAge.setText(if (person.age != 0 || person.id != 0L) person.age.toString() else "")

        btnRemove.visibility = if (person.id == 0L) View.GONE else View.VISIBLE

        btnCancel.setOnClickListener { dismiss() }
        btnSave.setOnClickListener { savePerson() }
        btnRemove.setOnClickListener { removePerson() }
    }

    private fun removePerson() {
       // viewModel.deletePerson(person)
        dismiss()
    }

    private fun savePerson() {
        person.firstName = edtFirstName.text.toString()
        person.lastName = edtLastName.text.toString()
        person.age = edtAge.text.toString().toInt()

       // viewModel.savePerson(person)
        dismiss()
    }

    companion object {
        val EXTRA_PERSON = "person"

        fun newInstance(person: Person) : PersonFormFragment {
            val args = Bundle()
            args.putSerializable(EXTRA_PERSON, person)

            val f = PersonFormFragment()
            f.arguments = args
            return f
        }
    }
}
