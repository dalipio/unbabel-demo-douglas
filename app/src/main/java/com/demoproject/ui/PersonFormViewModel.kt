package com.demoproject.ui

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.util.Log
import com.demoproject.data.AppDatabase
import com.demoproject.data.Post
import com.demoproject.data.PostDao


/**
 * Created by HAL-9000 on 21/08/2017.
 */
class PersonFormViewModel(app: Application) : AndroidViewModel(app) {

    private var db: PostDao? = null

    private fun postDao(): PostDao? {
        if (db == null) {
            db = AppDatabase.getDatabase(getApplication())?.peopleDao()
        }
        return db
    }

    fun savePerson(post: Post) {
        postDao()?.insertPerson(post)
        Log.e("test", "insert post: " + post.title)
    }
}