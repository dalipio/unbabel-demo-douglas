package com.demoproject.server


import com.demoproject.data.Post
import com.google.gson.Gson

/**
 * Created by HAL-9000 on 19/08/2017.
 */
class DataMapper {

    fun getPostsObjectFromJson(jsonStr: String): List<Post> {
        val posts: List<Post> = Gson().fromJson(jsonStr, Array<Post>::class.java).toList()
        return posts
    }

//    fun getUsersOjbectFromJson(jsonStr: String): List<User> {
//        val users: List<User> = Gson().fromJson(jsonStr, Array<User>::class.java).toList()
//        return users
//    }
//
//    fun getCommentsObjectFromJson(jsonStr: String): List<Comment> {
//        val comments: List<Comment> = Gson().fromJson(jsonStr, Array<Comment>::class.java).toList()
//        return comments
//    }
}