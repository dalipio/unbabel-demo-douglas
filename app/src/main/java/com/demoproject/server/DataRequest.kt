package com.demoproject.server

import java.net.URL

/**
 * Created by HAL-9000 on 19/08/2017.
 */
class DataRequest {

    companion object {
        private val URL_POSTS = "http://jsonplaceholder.typicode.com/posts"
        private val URL_USERS = "http://jsonplaceholder.typicode.com/users"
        private val URL_COMMENTS = "http://jsonplaceholder.typicode.com/comments"
    }

    fun executeFromPosts(): String {
        return URL(URL_POSTS).readText()
    }


    fun executeFromUsers(): String {
        return URL(URL_USERS).readText()
    }

    fun executeFromComments(): String {
        return URL(URL_COMMENTS).readText()
    }
}