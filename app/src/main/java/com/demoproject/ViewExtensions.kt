package com.demoproject

/**
 * Created by HAL-9000 on 07/08/2017.
 */
import android.content.Context
import android.view.View

val View.ctx: Context
    get() = context